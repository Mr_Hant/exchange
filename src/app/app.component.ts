import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'TestApp';

  constructor(
    translateService: TranslateService
  ) {
    translateService.setDefaultLang('ru');
    // translateService.setDefaultLang('en');
  }
}
