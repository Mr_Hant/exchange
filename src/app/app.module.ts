import {BrowserModule} from '@angular/platform-browser';
import {Injectable, NgModule} from '@angular/core';

import {AppRoutingModule} from './routing/app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {EffectsModule} from '@ngrx/effects';
import {CurrencyPipe, DatePipe, registerLocaleData} from '@angular/common';
import {TranslatePoHttpLoader} from '@biesbjerg/ngx-translate-po-http-loader';
import localeRu from '@angular/common/locales/ru';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {SharedModule} from './modules/shared/shared.module';
import {CachingInterceptorService} from './services/caching-interceptor.service';
import {MatPaginatorIntl} from '@angular/material';
import {CustomMatPaginatorIntl} from './services/custom-paginator-intl';

registerLocaleData(localeRu, 'ru');

export function createTranslateLoader(http: HttpClient) {
  return new TranslatePoHttpLoader(http, 'assets/i18n', '.po');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    StoreModule.forRoot({}),
    StoreDevtoolsModule.instrument({
      maxAge: 25
    }),
    EffectsModule.forRoot([]),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    SharedModule
  ],
  bootstrap: [AppComponent],
  providers: [
    DatePipe,
    CurrencyPipe,
    {
      provide: MatPaginatorIntl,
      useClass: CustomMatPaginatorIntl,
    },
    {provide: HTTP_INTERCEPTORS, useClass: CachingInterceptorService, multi: true}
  ]
})
export class AppModule {
}
