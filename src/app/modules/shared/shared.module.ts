import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule} from '@ngx-translate/core';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCommonModule, MatDatepickerModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule, MatNativeDateModule, MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSnackBarModule, MatSortModule, MatTableModule,
  MatToolbarModule,
  MatTooltipModule
} from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TranslateModule,
    MatCardModule,
    MatInputModule,
    MatTooltipModule,
    MatIconModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatButtonModule,
    MatCommonModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatDatepickerModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule
  ],
  exports: [
    TranslateModule,
    MatCardModule,
    MatInputModule,
    MatTooltipModule,
    MatIconModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatButtonModule,
    MatCommonModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatDatepickerModule,
    CdkTableModule,
    MatSortModule,
    MatPaginatorModule
  ]
})
export class SharedModule {
}
