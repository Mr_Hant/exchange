export interface HttpError {
  error: {
    statusCode: number,
    name: string,
    message: string,
    code: string
  };
}
