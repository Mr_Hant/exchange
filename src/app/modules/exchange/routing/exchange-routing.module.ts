import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ExchangePageComponent} from '../pages/exchange-page/exchange-page.component';
import {LoadAllExchangesGuard} from './guards/load-all-exchanges.guard';
import {LoadCurrentExchangeRateGuard} from './guards/load-current-exchange-rate.guard';

const routes: Routes = [
  {
    path: '',
    component: ExchangePageComponent,
    canActivate: [LoadAllExchangesGuard, LoadCurrentExchangeRateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExchangeRoutingModule {
}
