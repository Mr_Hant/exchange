import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {ExchangeState} from '../../store/reducers/exchange.reducer';
import {LoadRate} from '../../store/actions/exchange.actions';
import {CURRENCY_LIST} from '../../models/currency.model';

@Injectable({
  providedIn: 'root'
})
export class LoadCurrentExchangeRateGuard implements CanActivate {
  constructor(private store: Store<ExchangeState>) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let date = new Date();
    date.setDate(date.getDate() - 1);
    let currencyFrom = 'EUR';
    let currencyTo = 'USD';
    if (next.queryParams.rateDate) {
      const queryDate = Date.parse(next.queryParams.rateDate);
      date = !isNaN(queryDate) ? new Date(next.queryParams.rateDate) : date;
    }
    if (next.queryParams.currencyFrom && CURRENCY_LIST.includes(next.queryParams.currencyFrom)) {
      currencyFrom = next.queryParams.currencyFrom;
    }
    if (next.queryParams.currencyTo && CURRENCY_LIST.includes(next.queryParams.currencyTo)) {
      currencyTo = next.queryParams.currencyTo;
    }
    this.store.dispatch(new LoadRate(currencyFrom, currencyTo, date));
    return true;
  }

}
