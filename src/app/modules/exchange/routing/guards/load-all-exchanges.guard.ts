import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {ExchangeState} from '../../store/reducers/exchange.reducer';
import {LoadRates} from '../../store/actions/exchange.actions';

@Injectable({
  providedIn: 'root'
})
export class LoadAllExchangesGuard implements CanActivate {
  constructor(private store: Store<ExchangeState>) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (next.queryParams.tableDate) {
      const queryDate = Date.parse(next.queryParams.tableDate);
      if (!isNaN(queryDate)) {
        this.store.dispatch(new LoadRates(new Date(next.queryParams.tableDate)));
        return true;
      }
    }
    const date = new Date();
    date.setDate(date.getDate() - 1);
    this.store.dispatch(new LoadRates(date));

    return true;
  }
}
