import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CURRENCY_LIST, CurrencyRateModel} from '../models/currency.model';
import {combineLatest, forkJoin, Observable} from 'rxjs';
import {TableRateModel} from '../models/table-rate.model';
import {environment} from '../../../../environments/environment';
import {DatePipe} from '@angular/common';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {

  constructor(private http$: HttpClient,
              private datePipe: DatePipe) {
  }

  getTableByDate(date: Date): Observable<TableRateModel[]> {
    return combineLatest(
      CURRENCY_LIST.map(currency => this.http$
        .get<CurrencyRateModel>(`${environment.apiUrl}${this.datePipe.transform(date, 'yyyy-MM-dd')}?base=${currency}`))
    ).pipe(map((data: CurrencyRateModel[]) => {
      return data.reduce((prev: TableRateModel[], current: CurrencyRateModel) => {
        return prev.concat(Object.keys(current.rates)
          .map((currencyRateKey): TableRateModel =>
            ({
              currencyFrom: current.base,
              currencyTo: currencyRateKey,
              rate: current.rates[currencyRateKey]
            })));
      }, []);
    }));
  }

  getRate(currencyFrom: string, currencyTo: string, date: Date): Observable<TableRateModel> {
    return this.http$.get<CurrencyRateModel>(`${environment.apiUrl}${this.datePipe.transform(date, 'yyyy-MM-dd')}?base=${currencyFrom}`)
      .pipe(
        map((data: CurrencyRateModel): TableRateModel => {
          return {
            currencyFrom,
            currencyTo,
            rate: data.rates[currencyTo]
          };
        })
      );
  }
}
