import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {ExchangeState} from '../../store/reducers/exchange.reducer';

@Component({
  selector: 'app-exchange-page',
  templateUrl: './exchange-page.component.html',
  styleUrls: ['./exchange-page.component.scss']
})
export class ExchangePageComponent {

  constructor(private store: Store<ExchangeState>) {
  }

}
