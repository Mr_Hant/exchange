import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import { ExchangePageComponent } from './pages/exchange-page/exchange-page.component';
import { RatesTableComponent } from './component/rates-table/rates-table.component';
import { CurrentRateComponent } from './component/current-rate/current-rate.component';
import {ExchangeRoutingModule} from './routing/exchange-routing.module';
import {SharedModule} from '../shared/shared.module';
import {ReactiveFormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {ExchangeReducer} from './store/reducers/exchange.reducer';
import {EffectsModule} from '@ngrx/effects';
import {ExchangeEffects} from './store/effects/exchange.effects';
import { ExchangePipe } from './pipes/exchange.pipe';

@NgModule({
  declarations: [ExchangePageComponent, RatesTableComponent, CurrentRateComponent, ExchangePipe],
  imports: [
    CommonModule,
    ExchangeRoutingModule,
    StoreModule.forFeature('exchange', ExchangeReducer),
    EffectsModule.forFeature([ExchangeEffects]),
    SharedModule,
    ReactiveFormsModule
  ]
})
export class ExchangeModule { }
