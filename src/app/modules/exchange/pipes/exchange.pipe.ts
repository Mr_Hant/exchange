import { Pipe, PipeTransform } from '@angular/core';
import {CurrencyPipe} from '@angular/common';

@Pipe({
  name: 'exchange'
})
export class ExchangePipe implements PipeTransform {
  constructor(private currencyPipe: CurrencyPipe) {}

  transform(value: any, ...args: any[]): any {
    if ((value || value === 0) && args && args.length === 2) {
      return this.currencyPipe.transform(1, args[0], 'symbol', '1.0-0', 'ru') + ' = '
        + this.currencyPipe.transform(value, args[1], 'symbol', '1.4-4', 'ru');
    }
    return value;
  }

}
