export interface TableRateModel {
  currencyFrom: string;
  currencyTo: string;
  rate: number;
}
