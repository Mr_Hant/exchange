import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ExchangeState} from '../reducers/exchange.reducer';

const getExchangeState = createFeatureSelector<ExchangeState>('exchange');
export const isLoadingExchangeRates = createSelector(getExchangeState, (state: ExchangeState) => state.isLoadingRates);
export const isLoadingCurrentExchangeRate = createSelector(getExchangeState, (state: ExchangeState) => state.isLoadingCurrentRate);
export const getExchangeRates = createSelector(getExchangeState, (state: ExchangeState) => state.tableRates);
export const getCurrentExchangeRate = createSelector(getExchangeState, (state: ExchangeState) => state.currentRate);
export const getCurrentTableDate = createSelector(getExchangeState, (state: ExchangeState) => state.rateDate);
export const getCurrentRateData =  createSelector(getExchangeState, (state: ExchangeState) => ({date: state.tableRatesDate, currencyFrom: state.currencyFrom, currencyTo: state.currencyTo}));
