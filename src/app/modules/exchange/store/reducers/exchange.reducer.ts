import {ExchangeActions, ExchangeActionTypes} from '../actions/exchange.actions';
import {TableRateModel} from '../../models/table-rate.model';
import {act} from '@ngrx/effects';

export interface ExchangeState {
  isLoadingRates: boolean;
  isLoadingCurrentRate: boolean;
  tableRates: TableRateModel[];
  currentRate: number;
  rateDate: Date;
  currencyFrom: string;
  currencyTo: string;
  tableRatesDate: Date;
}

export const initialState: ExchangeState = {
  isLoadingCurrentRate: false,
  isLoadingRates: false,
  tableRates: [],
  currentRate: null,
  rateDate: null,
  currencyFrom: 'EUR',
  currencyTo: 'USD',
  tableRatesDate: null
};

export function ExchangeReducer(state = initialState, action: ExchangeActions): ExchangeState {
  switch (action.type) {
    case ExchangeActionTypes.LoadRates:
      return {
        ...state,
        isLoadingRates: true,
        tableRatesDate: action.payload
      };
    case ExchangeActionTypes.LoadRatesSuccess:
      return {
        ...state,
        isLoadingRates: false,
        tableRates: action.payload
      };
    case ExchangeActionTypes.LoadRatesFail:
      return {
        ...state,
        isLoadingRates: false
      };
    case ExchangeActionTypes.LoadRate:
      return {
        ...state,
        isLoadingCurrentRate: true,
        rateDate: action.date,
        currencyFrom: action.currencyFrom,
        currencyTo: action.currencyTo
      };
    case ExchangeActionTypes.LoadRateSuccess:
      return {
        ...state,
        isLoadingCurrentRate: false,
        currentRate: action.payload.rate
      };
    case ExchangeActionTypes.LoadRateFail:
      return {
        ...state,
        isLoadingCurrentRate: false
      };
    default:
      return state;
  }
}
