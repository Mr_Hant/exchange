import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {
  ExchangeActionTypes,
  LoadRate,
  LoadRateFail,
  LoadRates,
  LoadRatesFail,
  LoadRatesSuccess,
  LoadRateSuccess
} from '../actions/exchange.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {ExchangeService} from '../../services/exchange.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ErrorHandlerService} from '../../../../services/error-handler.service';
import {of} from 'rxjs';


@Injectable()
export class ExchangeEffects {
  constructor(private actions$: Actions,
              private exchangeService: ExchangeService,
              private errorHandler: ErrorHandlerService) {
  }

  @Effect()
  loadExchangeRates$ = this.actions$.pipe(
    ofType(ExchangeActionTypes.LoadRates),
    switchMap((action: LoadRates) => {
      return this.exchangeService.getTableByDate(action.payload).pipe(
        map(data => {
          return new LoadRatesSuccess(data);
        }),
        catchError((error: HttpErrorResponse) => {
          this.errorHandler.handle(error);
          return of(new LoadRatesFail());
        })
      );
    })
  );

  @Effect()
  loadExchangeRate$ = this.actions$.pipe(
    ofType(ExchangeActionTypes.LoadRate),
    switchMap((action: LoadRate) => {
      return this.exchangeService.getRate(action.currencyFrom, action.currencyTo, action.date)
        .pipe(
          map(data => new LoadRateSuccess(data)),
          catchError((error: HttpErrorResponse) => {
            this.errorHandler.handle(error);
            return of(new LoadRateFail());
          })
        );
    })
  );
}
