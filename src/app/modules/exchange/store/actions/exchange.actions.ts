import {Action} from '@ngrx/store';
import {TableRateModel} from '../../models/table-rate.model';

export enum ExchangeActionTypes {
  LoadRates = '[Exchange] Load Rates',
  LoadRatesSuccess = '[Exchange] Load Rates Success',
  LoadRatesFail = '[Exchange] Load Rates Fail',

  LoadRate = '[Exchange] Load Rate',
  LoadRateSuccess = '[Exchange] Load Rate Success',
  LoadRateFail = '[Exchange] Load Rate Fail',
}

export class LoadRates implements Action {
  readonly type = ExchangeActionTypes.LoadRates;

  constructor(public payload: Date) {
  }
}

export class LoadRatesSuccess implements Action {
  readonly type = ExchangeActionTypes.LoadRatesSuccess;

  constructor(public payload: TableRateModel[]) {
  }
}

export class LoadRatesFail implements Action {
  readonly type = ExchangeActionTypes.LoadRatesFail;

  constructor() {
  }
}

export class LoadRate implements Action {
  readonly type = ExchangeActionTypes.LoadRate;

  constructor(public currencyFrom: string, public currencyTo: string, public date: Date) {
  }
}

export class LoadRateSuccess implements Action {
  readonly type = ExchangeActionTypes.LoadRateSuccess;

  constructor(public payload: TableRateModel) {
  }
}

export class LoadRateFail implements Action {
  readonly type = ExchangeActionTypes.LoadRateFail;
}


export type ExchangeActions =
  | LoadRates
  | LoadRatesSuccess
  | LoadRatesFail
  | LoadRate
  | LoadRateSuccess
  | LoadRateFail;
