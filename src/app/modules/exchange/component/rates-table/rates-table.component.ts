import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TableRateModel} from '../../models/table-rate.model';
import {Observable, Subject} from 'rxjs';
import {ExchangeState} from '../../store/reducers/exchange.reducer';
import {select, Store} from '@ngrx/store';
import {getCurrentTableDate, getExchangeRates, isLoadingExchangeRates} from '../../store/selectors/exchange.selectors';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FormControl} from '@angular/forms';
import {distinctUntilChanged, take, takeUntil} from 'rxjs/operators';
import {LoadRates} from '../../store/actions/exchange.actions';
import {ActivatedRoute, Router} from '@angular/router';

export const PAGE_SIZE = 10;

@Component({
  selector: 'app-rates-table',
  templateUrl: './rates-table.component.html',
  styleUrls: ['./rates-table.component.scss']
})
export class RatesTableComponent implements OnInit, OnDestroy {
  dateTimeControl: FormControl = new FormControl();
  maxDate: Date;
  dataSource: MatTableDataSource<TableRateModel>;
  isLoading$: Observable<boolean>;
  subscriptions$ = new Subject();
  @ViewChild(MatPaginator, null) paginator: MatPaginator;
  @ViewChild(MatSort, null) sort: MatSort;
  pageSize = PAGE_SIZE;
  pageSizeOptions: number[] = [1, 5, 10, 15, 20];
  displayedColumns = ['currencyFrom', 'currencyTo', 'rate'];

  constructor(private store: Store<ExchangeState>,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    this.maxDate = date;

    this.store.pipe(select(getExchangeRates), takeUntil(this.subscriptions$))
      .subscribe((data) => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.filterPredicate =
          (predicateData: TableRateModel, filter: string): boolean => predicateData.currencyFrom.toLowerCase().includes(filter);
      });

    this.isLoading$ = this.store.pipe(select(isLoadingExchangeRates));

    this.store.pipe(select(getCurrentTableDate), take(1))
      .subscribe((currentDate) => {
        this.dateTimeControl.setValue(currentDate, {emitEvent: false});
      });

    this.dateTimeControl.valueChanges.pipe(
      takeUntil(this.subscriptions$),
      distinctUntilChanged()
    ).subscribe((dateValue: Date) => {
      this.router.navigate([],
        {
          relativeTo: this.route,
          queryParams: {tableDate: dateValue.toDateString()},
          queryParamsHandling: 'merge'
        });
      this.store.dispatch(new LoadRates(dateValue));
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy(): void {
    this.subscriptions$.next();
    this.subscriptions$.complete();
  }
}
