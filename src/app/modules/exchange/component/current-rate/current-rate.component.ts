import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {distinctUntilChanged, map, startWith, take, takeUntil} from 'rxjs/operators';
import {LoadRate} from '../../store/actions/exchange.actions';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {ExchangeState} from '../../store/reducers/exchange.reducer';
import {select, Store} from '@ngrx/store';
import {getCurrentExchangeRate, getCurrentRateData, isLoadingCurrentExchangeRate} from '../../store/selectors/exchange.selectors';
import {CURRENCY_LIST} from '../../models/currency.model';

@Component({
  selector: 'app-current-rate',
  templateUrl: './current-rate.component.html',
  styleUrls: ['./current-rate.component.scss']
})
export class CurrentRateComponent implements OnInit, OnDestroy {
  form: FormGroup;
  maxDate: Date;
  isLoading$: Observable<boolean>;
  filteredCurrencyFrom$: Observable<string[]>;
  filteredCurrencyTo$: Observable<string[]>;
  subscriptions$ = new Subject();
  currentRate$: Observable<number>;

  constructor(private fb: FormBuilder,
              private router: Router,
              private store: Store<ExchangeState>,
              private route: ActivatedRoute) {
    this.form = this.fb.group({
      date: [new Date(), [Validators.required]],
      currencyFrom: [null, [Validators.required]],
      currencyTo: [null, [Validators.required]],
    });
  }

  ngOnInit() {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    this.maxDate = date;

    this.currentRate$ = this.store.pipe(select(getCurrentExchangeRate));
    this.isLoading$ = this.store.pipe(select(isLoadingCurrentExchangeRate));

    this.store.pipe(select(getCurrentRateData), take(1))
      .subscribe((currentDate) => {
        this.form.setValue(currentDate, {emitEvent: false});
      });

    this.form.valueChanges.pipe(
      takeUntil(this.subscriptions$),
      distinctUntilChanged()
    ).subscribe((formValue) => {
      this.router.navigate([],
        {
          relativeTo: this.route,
          queryParams: {
            rateDate: formValue.date.toDateString(),
            currencyFrom: formValue.currencyFrom,
            currencyTo: formValue.currencyTo
          },
          queryParamsHandling: 'merge'
        });
      this.store.dispatch(new LoadRate(formValue.currencyFrom, formValue.currencyTo, formValue.date));
    });

    this.filteredCurrencyFrom$ = this.form.get('currencyFrom').valueChanges
      .pipe(startWith(''),
        map(currency => currency ? this.filterCurrency(currency) : CURRENCY_LIST.slice()));
    this.filteredCurrencyTo$ = this.form.get('currencyTo').valueChanges
      .pipe(startWith(''),
        map(currency => currency ? this.filterCurrency(currency) : CURRENCY_LIST.slice()));
  }

  filterCurrency(name: string): string[] {
    return CURRENCY_LIST.filter(currency =>
      currency.toLowerCase().includes(name.toLowerCase()));
  }

  ngOnDestroy(): void {
    this.subscriptions$.next();
    this.subscriptions$.complete();
  }

  clearFrom() {
    this.form.patchValue({currencyFrom: ''});
  }

  clearTo() {
    this.form.patchValue({currencyTo: ''});
  }

  swap() {
    const fromData = this.form.getRawValue();
    this.form.patchValue({
      currencyFrom: fromData.currencyTo,
      currencyTo: fromData.currencyFrom
    });
  }
}
