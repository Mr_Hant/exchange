import {Injectable} from '@angular/core';
import {MatPaginatorIntl} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {_} from '../modules/shared/helpers/translation-marker';

@Injectable({
  providedIn: 'root'
})
export class CustomMatPaginatorIntl extends MatPaginatorIntl {
  constructor(private translateService: TranslateService) {
    super();
  }

  itemsPerPageLabel = this.translateService.instant(_('Items per page:'));
  nextPageLabel = this.translateService.instant(_('Next'));
  previousPageLabel = this.translateService.instant(_('Previous'));

  getRangeLabel = function(page: number, pageSize: number, length: number) {
    if (length === 0 || pageSize === 0) {
      return '0 из ' + length;
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return startIndex + 1 + ' - ' + endIndex + ' из ' + length;
  };
}
