import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {
  constructor(private _snackBar: MatSnackBar,
              private translateService: TranslateService) {
  }

  handle(httpError: any) {
  }
}
