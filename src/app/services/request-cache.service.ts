import {Injectable} from '@angular/core';
import {HttpRequest, HttpResponse} from '@angular/common/http';

const maxAge = 100000;
const STORAGE_KEY = 'exchange-map';

@Injectable({
  providedIn: 'root'
})
export class RequestCacheService {
  cache = new Map();

  constructor() {
  }

  get(req: HttpRequest<any>): HttpResponse<any> | undefined {
    const url = req.urlWithParams;
    const cached = this.cache.get(url);

    if (!cached) {
      return undefined;
    }

    return cached.response;
  }

  put(req: HttpRequest<any>, response: HttpResponse<any>): void {
    const url = req.url;
    const entry = {url, response, lastRead: Date.now()};
    this.cache.set(url, entry);

    const expired = Date.now() - maxAge;
    this.cache.forEach(expiredEntry => {
      if (expiredEntry.lastRead < expired) {
        this.cache.delete(expiredEntry.url);
      }
    });
  }
}
